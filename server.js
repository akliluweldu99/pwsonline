var http = require('http')
var static = require('node-static')

console.log('PWS Online Project')

var httpServer = http.createServer()
var fileServer = new static.Server('./public')

var data = {
    "firstName": "Mariusz",
    "lastName": "Jarocki",
    "attempt": 0
}

httpServer.on('request', function(req, res) {
        console.log(req.method, req.url)
        if(req.url == '/data') {
            data.attempt-=10;
            res.writeHead(200, 'OK', { contentType: 'application/json' })
            res.write(JSON.stringify(data))
            res.end()
        } if(req.url == '/zero') {
            data.attempt = 0
            res.writeHead(200, 'OK', { contentType: 'application/json' })
            res.write(JSON.stringify(data))
            res.end()
        } else {
            fileServer.serve(req, res)
        }
    }
)

httpServer.listen(8888)